# Cornell ASIST Lightweight Testbed 

The Cornell ASIST Lightweight Testbed enables ASIST performers quickly deploy small-scale experiments to test preliminary hypotheses and research questions. It has the following features:

- **Quick setup** and web deployment
- **Two simultaneous players** with low latency
- Automatic loading of ASIST-style **semantic maps**
- Easily **configurable game settings** (rescue scoring)
- Easily editable **embedded questinonaires and informed consent**
- Some **player-to-player communication** (needs coding to customize)

![GIF Missing](docs/cornell_asist_survey_game.gif)



## Installation
Find instructions for installing the testbed on your machine [here](./docs/local_installation.md).

## Running the Testbed
After completing the installation instructions above, use the instructions [here](./docs/local_run.md) to run the testbed on your machine. 

## Web Hosting
Once you've gotten the lightweight testbed running on your machine using the above instructions, you may wish to use web-hosting features to deploy the testbed on a web application. For the most part, deploying locally and on a web-facing server, where you have root access, is not any different. Some things to keep in mind:

1. You need to allow incoming TCP requests to ports 8083 and 8084 (by default) or configure some firewall / forwarding scheme on your server. 
2. If you are planning to run over HTTPS/WSS (which is the default configuration) - you need install an SSL certificate on the server.
3. You need to store the Firebase’s credential json file, and all of the certificate files and set up the environment variables to point to the correct file location.  

See this [document](./docs/web_hosting.md) for alternative (and possibly easier) options of running the testbed on the web. 

## Testbed Customizations
See this [document](./docs/testbed_custom.md) for some ways that you can customize your testbed. Make sure that you rerun the testbed after making your changes.  

## Testbed Architecture
For more information about the testbed architecture, please see [this document](./docs/testbed_arch.md)

## Acknowledgements
We would like to acknoledge this work was forked from the [Rutgers ASIST Client](https://github.com/CoDaS-Lab/ASIST-Client) and [Rutgers ASIST API](https://github.com/CoDaS-Lab/ASIST-API) from Patrick Shafto's [Cognitive and Data Science Lab](http://shaftolab.com/) at Rutgers University.

This material is based upon work supported by the Defense Advanced Research Projects Agency (DARPA) under Contract No. W911NF2010004. Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the Defense Advanced Research Projects Agency (DARPA).

