# ASIST-CLIENT
This client is part of a game built for Cornell Lightweight testbed for ASIST project using Phaser.js. The Client works with the API available at [ASIST-API](https://gitlab.com/cu_asist/testbed/asist_api)

## Requirements:
- Node 12.8.3+ installed

## Running Locally
- Install node modules using `npm install` from the current directory of the project
- Set up the `socketURL`  variable located  in `/public/js/config.js` file.
- Start node server using `node localServer.js` and it should be should be available at `http://localhost:880`


<!-- tile extruder -->
