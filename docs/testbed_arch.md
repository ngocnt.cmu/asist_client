# Cornell Lightweigh Testbed Architecture

![Testbed Architecture](./ASIST-Testbed.png)

<div align="center">
  Figure 1. Testbed architecture
</div>

The testbed is designed as a modular and flexible codebase that can be modified and upgraded based on the user's needs, as shown in the above diagram.

## Code Repositories
The code is distributed as two separate repositories, each indicated with dashed lines:

### ASIST-Client 
This is a [`node`](https://nodejs.org/) javascript web server, using the [`express`](https://expressjs.com/) framework. It serves the pages of the game using the [`pug`](https://pugjs.org/) templating engine. 

The `pug` engine allows flexible organization with content saved in markdown and pages organized in simple view files.

The client-side game logic is implemented using the [`phaser`](https://phaser.io/) javascript game engine. This allows for smooth user input and animation and contains all of the logic in a javascript and assets directory.

Both the phaser and the web components communicate using websockets with the ASIST-API side of the testbed.

### ASIST API 

The purpose of this codebase is to coordinate and persiste game states between the clients. It is built as a [`Uvicorn`/`FastAPI`](https://www.uvicorn.org/#fastapi) websocket `REST` server, and persists temporary data using [`REDIS`](https://redis.com/). In addition, it stores longer-term data on a [`Firebase`](https://firebase.google.com/) cloud database.

## Physical Devices

Physically, the testbed runs in three locations (indicated by solid lines in the diagram above):

### Player computers

Render the web pages and run the `phaser` JS game engine code

### Server

Runs the `node` server, the `uvicorn` server, and the `REDIS` database, as described in (local_run.md)[the local run] instructions. Of course these components could be run on separate servers, if the requirements of the deployment require it.

### Firebase

This is a cloud database hosted by Google, and runs on a separate machine.
