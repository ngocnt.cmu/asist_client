# Testbed Customizations

The current purpose of the testbed is to act as a lightweight proxy to the Minecraft ASIST search-and-rescue simulation. In the case that certain parameters need to be changed, please see the below instructions for customizing the testbed for your purposes. We understand that these customizations may not be exhaustive, so please contact us if you need help adjusting the testbed.

Most of the changes in the game state can be found in the asist-client repository. We will call that path `$CLIENT_DIR`

## Load a New Semantic Map
1. Copy the semantic map file to the `$CLIENT_DIR/assets/semantic_maps` directory.
2. Edit the `semantic_map_file` field in the `$CLIENT_DIR/public/js/config.js` file to point to your map file. 

## Adjust the Game Timer
1. Edit the `game_time_seconds` in the `$CLIENT_DIR/public/js/config.js` file to the desired duration of the game.

## Adjust the Victim Scoring
1. Navigate to where you saved the `asist-client` repository. 
2. Open the file `asist-client/public/js/config.js` and change the fields `green_victim_point` and/or `yellow_victim_point` to be the desired point valuation for those victims in the game.

## Adjust the Number of Victims
1. Navigate to where you saved the `asist-client` repository. 
2. Open the file `asist-client/public/js/config.js` and change the fields `green_victim_count` and/or `yellow_victim_count` to be the desired number of victims for each respective victim type. 
Currently, we limit the number of victims for each type to 17, although this may be updated in 
a future iteration. 

## Add New Instructions 
1. Edit the `instructions.md` file in the `$CLIENT_DIR/assets/pages` directory. 

## Change the Consent Form
1. Edit the `consent-form.md` file in the `$CLIENT_DIR/assets/pages` directory. 
2. Remember that the current `consent-form.md` file has the contact information set the Cornell ASIST team.  

## Adding a New Presurvey
1. Create a new survey using [Survey JS](https://surveyjs.io/create-survey-v2). 
2. Put the js survey file that you generated into the `$CLIENT_DIR/public/js/surveys` directory. 
3. Open the js file. Make sure that the variable name of the survey is set to `pre_survey `.
4. Add the following line to the end of the file: `export {pre_survey};`
5. Edit the `pre_survey` field in the `$CLIENT_DIR/public/js/config.js` file to point to your survey file. 

## Adding a New Postsurvey
1. Create a new survey using [Survey JS](https://surveyjs.io/create-survey-v2). 
2. Put the js survey file that you generated into the `$CLIENT_DIR/public/js/surveys` directory. 
3. Open the js file. Make sure that the variable name of the survey is set to `post_survey `.
4. Add the following line to the end of the file: `export {post_survey};`
5. Edit the `post_survey` field in the `$CLIENT_DIR/public/js/config.js` file to point to your survey file. 

## More Documentation 
Find more complete documentation of the Testbed API [here](https://docs.google.com/document/d/15zH-1OvOYjZGLO59F19nBD0pDlMkGhqT_ZL-bb4CZx8/edit#heading=h.qiv9jo2b9ejt)
