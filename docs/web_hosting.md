# Alternative Web Hosting Options

For the most part, deploying locally and on a web-facing server is not any different, except the need to configure your firewall / NAT / port forwarding based on your deployment environment. 

Below are some additional options that might be easier for some users:

## Pseudo-Local with ngrok 
1. Create a ngrok account (you will need the paid version)
2. Create two subdomains
3. Copy authtoken
4. In your ngrok config file add (more about this configuration file):
```
authtoken: [PASTE authtoken]
region: us
tunnels:
  asist-api:
    addr: 8084
    proto: http
    Hostname: subdomain1.ngrok.io
  asist-client:
    addr: 8083
    proto: http
    Hostname: subdomain2.ngrok.io


```

## Heroku (untested)
These instructions are untested. Please write to us if any of them need to be updated. 
### Set Up FireBase
1. Create Project in Firebase account | Installation & Setup for REST API | Firebase
2. SDK Setup | Add the Firebase Admin SDK to your server
### Set Up API on Heroku
1. Create APP on Heroku and Add Heroku REDIS add-on | Heroku Redis | Heroku Dev Center
2. Connect API repository Link
3. Set up the environment variables **REDIS_URL**, **FIREBASE_URL** and **GOOGLE_APPLICATION_CREDENTIALS**. The last variable is the path to the json file that contains Firebase database account authentication credentials.
4.Deploy
### Set Up Client on Heroku
1. Create APP on Heroku
2. Connect Client repository Link
3. Deploy

Heroku CLI tools can also be used for deployment from the command line.
