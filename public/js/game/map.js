import { ArraySet } from "/js/utils.js";

export default class Map {

    constructor(filename) {

        var mapObj = this;

        console.log("Loading Map");

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log("Parsing Map JSON");
                var semantic_map = JSON.parse(this.responseText);
                mapObj._handle_semantic_map(semantic_map)
            }
        }
        xmlhttp.open("GET", filename, true);
        xmlhttp.send();
    }

    _handle_semantic_map(semMap) {

        // grab all the coordinates
        var Xs = [];
        var Zs = [];

        this._extractXZCoords(semMap.locations, Xs, Zs);
        this._extractXZCoords(semMap.connections, Xs, Zs);
        this._extractXZCoords(semMap.objects, Xs, Zs);

        this.minX = Math.min.apply(Math, Xs);
        this.maxX = Math.max.apply(Math, Xs);
        this.minZ = Math.min.apply(Math, Zs);
        this.maxZ = Math.max.apply(Math, Zs);

        this.width = Math.abs(this.maxX - this.minX);
        this.height = Math.abs(this.maxZ - this.minZ);

        this.noncollision_type_set = new Set(semMap.noncollision_types);
        this.semantic_map = semMap;
    }

    _extractXZCoords(collection, Xs, Zs) {
        collection.forEach((item) => {
            if (item.bounds) {
                item.bounds.coordinates.forEach((coord) => {
                    Xs.push(coord.x);
                    Zs.push(coord.z);
                });
            }
        });
    }

    get_rect_details(coordinates, minX, minZ) {
        var point1 = coordinates[0];
        var point2 = coordinates[1];

        var tileX = Math.abs(minX - point1.x);
        var tileY = Math.abs(minZ - point1.z);
        var width = Math.abs(point2.x - point1.x);
        var height = Math.abs(point2.z - point1.z);

        return [tileX, tileY, width, height];
    }

    fill_rect(layer, coordinates, tileset_index, used_tiles_set, minX, minZ) {
        var data = this.get_rect_details(coordinates, minX, minZ);
        var tileX = data[0];
        var tileY = data[1];
        var width = data[2];
        var height = data[3];

        layer.fill(tileset_index, tileX, tileY, width, height);

        //saved used tiles
        for (var x_temp = tileX; x_temp < tileX + width; x_temp++) {
            for (var y_temp = tileY; y_temp < tileY + height; y_temp++) {
                used_tiles_set.add([x_temp, y_temp]);
            }
        }
    }

    get_block_details(coordinates, minX, minZ) {
        var point = coordinates[0];

        var tileX = Math.abs(minX - point.x);
        var tileY = Math.abs(minZ - point.z);

        return [tileX, tileY];
    }

    fill_block(layer, obj, tileset_index, used_tiles_set, minX, minZ) {
        var data = this.get_block_details(obj, minX, minZ);
        var tileX = data[0];
        var tileY = data[1];

        layer.putTileAt(tileset_index, tileX, tileY);
        // save used tile
        used_tiles_set.add([tileX, tileY]);
    }

    get_color_index(obj, type_colors) {
        var tileset_index = type_colors[String(obj.type)];
        return tileset_index;
    }

    //create walls
    map_walls(
        layer,
        type_colors,
        width_in_tiles,
        height_in_tiles,
        used_tiles_set
    ) {
        // create set of all tiles
        var all_tiles_set = new ArraySet();
        for (var x_temp = 0; x_temp < width_in_tiles; x_temp++) {
            for (var y_temp = 0; y_temp < height_in_tiles; y_temp++) {
                all_tiles_set.add([x_temp, y_temp]);
            }
        }
        // unused tiles  --> take the difference between the set of all tiles and use tiles
        var unused_tiles_set = new Set(
            [...all_tiles_set].filter((x) => !used_tiles_set.has(x))
        );

        // convert set of unused tiles to array
        var unused_tiles = Array.from(unused_tiles_set, (x) =>
            x.split(",").map(Number)
        );

        var tileset_index = type_colors["wall"];

        // fill each unused tile
        for (var i = 0; i < unused_tiles.length; i++) {
            var x = unused_tiles[i][0];
            var y = unused_tiles[i][1];
            layer.putTileAt(tileset_index, x, y);
        }
    }

    // LOCATION
    map_all_locations(
        static_noncollision_layer,
        static_collision_layer,
        locations,
        type_colors,
        noncollision_type_set,
        used_tiles_set,
        minX,
        minZ
    ) {
        // console.log("LOCATIONS")
        locations.forEach((obj) => {
            if (obj.bounds && obj.bounds.type == "rectangle"  && obj.type != "yard_part" && obj.type != "fountain_part" && obj.type != "walkway" ) {
                if (noncollision_type_set.has(obj.type)) {
                    this.fill_rect(
                        static_noncollision_layer,
                        obj.bounds.coordinates,
                        this.get_color_index(obj, type_colors),
                        used_tiles_set,
                        minX,
                        minZ
                    );
                } else {
                    this.fill_rect(
                        static_collision_layer,
                        obj.bounds.coordinates,
                        this.get_color_index(obj, type_colors),
                        used_tiles_set,
                        minX,
                        minZ
                    );
                }
            } else if (obj.bounds && obj.bounds.type == "block") {
                console.log("Location has block");
            }
        });

        return true;
    }

    // CONNECTIONS
    map_all_connections(
        static_noncollision_layer,
        static_collision_layer,
        connections,
        type_colors,
        noncollision_type_set,
        used_tiles_set,
        minX,
        minZ
    ) {
        // console.log("CONNECTIONS")
        connections.forEach((obj) => {
            if (obj.bounds && obj.bounds.type == "rectangle") {
                if (noncollision_type_set.has(obj.type)) {
                    this.fill_rect(
                        static_noncollision_layer,
                        obj.bounds.coordinates,
                        this.get_color_index(obj, type_colors),
                        used_tiles_set,
                        minX,
                        minZ
                    );
                } else {
                    this.fill_rect(
                        static_collision_layer,
                        obj.bounds.coordinates,
                        this.get_color_index(obj, type_colors),
                        used_tiles_set,
                        minX,
                        minZ
                    );
                }

                // console.log("AYE")
            }
        });

        return true;
    }

    // OBJECT treated as layers
    map_all_objects(
        static_noncollision_layer,
        static_collision_layer,
        objects,
        type_colors
    ) {
        // console.log("OBJECTS")
        objects.forEach((obj) => {
            if (obj.bounds && obj.bounds.type == "block") {
                fill_block(
                    static_collision_layer,
                    obj.bounds.coordinates,
                    this.get_color_index(obj, type_colors),
                    used_tiles_set
                );
            }
        });
    }

    // OBJECT treated as objects
    obj_all_objects(
        physics_group,
        type_name,
        objects,
        tile_width,
        tile_height,
        minX,
        minZ,
        id_set,
        obj_count=Infinity
    ) {
        //counter
        var i = 0;
        objects.forEach((obj) => {
            if (obj.bounds && obj.bounds.type =="block" && obj.type ==type_name && i<obj_count){
                var xy = this.get_block_details(obj.bounds.coordinates,minX,minZ)
                // create physics obj 
                var phyObj = physics_group
                    .create(xy[0] * tile_width, xy[1] * tile_height, type_name)
                    .setOrigin(0, 0);
                phyObj.setScale(0.10, 0.10);
    
                // objects light
                phyObj.setPipeline('Light2D');
                // console.log(obj.id) 'vg5'
                phyObj.id = obj.id
                id_set.add(obj.id)
                //update counter
                i += 1;
            }  
        });
    }

}