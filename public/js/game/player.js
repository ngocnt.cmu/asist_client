export default class Player {
    constructor(game_scene, config) {
        this.game_scene = game_scene
        this.config = config
        this.x = config.x
        this.y = config.y
        this.x_vel = 0;
        this.y_vel = 0;
        this.name = config.name
        this.obj = game_scene.physics.add.sprite(config.x, config.y, config.name);
        this.obj.setScale(config.scale);
        // this.game_scene.gameconfig.dTime = new Date().toISOString();



        this.game_scene.anims.create({
            "key": this.name + "left",
            "frames": [
                { "key": this.name, "frame": 0 }
            ],
            "repeat": -1
        });
        this.game_scene.anims.create({
            "key": this.name + "down",
            "frames": [
                { "key": this.name, "frame": 1 }
            ],
            "repeat": -1
        });
        this.game_scene.anims.create({
            "key": this.name + "right",
            "frames": [
                { "key": this.name, "frame": 2 }
            ],
            "repeat": -1
        });
        this.game_scene.anims.create({
            "key": this.name + "up",
            "frames": [
                { "key": this.name, "frame": 3 }
            ],
            "repeat": -1
        });

        this.obj.anims.play(this.name + "down");
    }

    move(x_vel, y_vel, direction) {
        this.obj.setVelocityX(x_vel);
        this.obj.setVelocityY(y_vel);
        this.obj.anims.play(direction);

        this.x = this.obj.body.position.x
        this.y = this.obj.body.position.y
    }
}