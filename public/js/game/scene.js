import { ArraySet } from "/js/utils.js"

import Player from "/js/game/player.js"
import GameState from "/js/game/state.js"

export default class ASISTScene extends Phaser.Scene {
    constructor(config) {
        super(config.testbed.phaser_config);

        this.config = config;
        this.testbed = config.testbed;
        this.game = config.game;
        this.map = config.testbed.map;
        this.semantic_map = config.testbed.map.semantic_map;
        this.mapConfig = {};
        this.mapConfig["scene"] = this;

        this.room_id = config.room_id;
        this.player_id = config.player_id;
        this.player_limit = config.player_limit;

        this.asist_timer = config.testbed.asist_timer;
        this.tile_size = config.testbed.tile_size;

        this.user_config = config.testbed.user_config;
        this.speed = config.testbed.user_config.speed;

        this.game_setup = config.testbed.game_setup;
        this.min_distance_away_from_victim_to_save = this.game_setup.min_distance_away_from_victim_to_save;

        this.player_score = 0;
        this.teammate_score = 0;
        this.player_med_kits_used = 0;
        this.teammate_med_kits_used = 0;
        this.player_green_saved  = 0;
        this.player_yellow_saved = 0;
        this.teammate_green_saved  = 0;
        this.teammate_yellow_saved = 0;
        
        this.used_tiles_set = new ArraySet();
        // list of players in the game 
        this.player_list = Array.from(Array(this.player_limit).keys());
        // player types and general color 
        this.agent_type_list = ["green_player", "yellow_player", "yellow_player"];
        this.agent_color = {
            "green_player": "#2A9D8F",
            "yellow_player": "#E9C46A"
        }

        this.teammate_id = -1;
        this.socket = this.config.socket;


        this.color = this.agent_color[this.agent_type_list[this.player_id]];
        document.getElementById("top-grid-item").style.backgroundColor   = this.color;
        document.getElementById("side-grid-item").style.backgroundColor  = this.color;
        document.getElementById("game-grid-item").style.backgroundColor  = this.color;
        document.getElementById("game").style.backgroundColor  = this.color;
        
    }

    preload() {
        // same preload stuff
        this.load.image('tiles', 'assets/tilesets/falcon_tileset_small_extruded.png');
        this.load.image('green_victim', 'assets/images/green_victim.png');
        this.load.image('green_victim_saved', 'assets/images/green_victim_saved.png');
        this.load.image('green_victim_dead', 'assets/images/green_victim_dead.png');
        this.load.image('yellow_victim', 'assets/images/yellow_victim.png');
        this.load.image('yellow_victim_saved', 'assets/images/yellow_victim_saved.png');
        this.load.image('yellow_victim_dead', 'assets/images/yellow_victim_dead.png');
        this.load.spritesheet("green_player", '/assets/images/green_player.png', { frameWidth: 27, frameHeight: 40 });
        this.load.spritesheet("yellow_player", '/assets/images/yellow_player.png', { frameWidth: 27, frameHeight: 40 });
    }

    create() {
        /*------------------CREATE WORLD MAP USING SEMANTIC MAP---------------------- */
        this.game_state = new GameState(this.mapConfig)
        
        /*============= MAP LOCATIONS AND CONNECTIONS =============== */
        // create tile map obj
        var map = this.make.tilemap({
            tileWidth: this.tile_size,
            tileHeight: this.tile_size,
            width: this.map.width,
            height: this.map.height,
        })

        //create tile set
        var tileset = map.addTilesetImage("tiles", null, this.tile_size, this.tile_size, 1, 2);

        // created layers for different kinds of tiles using tileset
        var static_wall_layer = map.createBlankLayer('static_background_layer', tileset);
        var static_noncollision_layer = map.createBlankLayer('static_noncollision_layer', tileset);
        var static_collision_layer = map.createBlankLayer('static_collision_layer', tileset);


        // lighting pipeline
        static_noncollision_layer.setPipeline('Light2D');
        static_collision_layer.setPipeline('Light2D');
        static_wall_layer.setPipeline('Light2D');

        // call respective functions to parse the semantic map 
        this.map.map_all_locations(static_noncollision_layer, 
                            static_collision_layer, 
                            this.semantic_map.locations, 
                            this.semantic_map.type_colors, 
                            this.map.noncollision_type_set, 
                            this.used_tiles_set, 
                            this.map.minX, 
                            this.map.minZ);
        this.map.map_all_connections(static_noncollision_layer, 
                            static_collision_layer, 
                            this.semantic_map.connections, 
                            this.semantic_map.type_colors, 
                            this.map.noncollision_type_set, 
                            this.used_tiles_set, 
                            this.map.minX, 
                            this.map.minZ);
        this.map.map_walls(static_wall_layer, 
                            this.semantic_map.type_colors, 
                            this.map.width, 
                            this.map.height, 
                            this.used_tiles_set);


        // set collistion for static collision and wall layers
        static_collision_layer.setCollisionByExclusion(-1, true);
        static_wall_layer.setCollisionByExclusion(-1, true);

        /*============= ADD OBJECTS (victims) =============== */
        // set for all the ids of the victims
        this.victims_not_saved_ids = new Set()
        this.victims_saved_ids = new Set()

        // create physics group for each victim type
        this.green_victims = this.physics.add.group({
            allowGravity: false,
            immovable: true
        });
        this.yellow_victims = this.physics.add.group({
            allowGravity: false,
            immovable: true
        });

        // // populate each victim physics group with victims from semantic map
        this.map.obj_all_objects(this.green_victims, "green_victim", 
                                    this.semantic_map.objects, 
                                    this.tile_size, this.tile_size, 
                                    this.map.minX, this.map.minZ, 
                                    this.victims_not_saved_ids, 
                                    this.user_config.green_victim_count);
        this.map.obj_all_objects(this.yellow_victims, "yellow_victim", 
                                    this.semantic_map.objects, 
                                    this.tile_size, this.tile_size, 
                                    this.map.minX, this.map.minZ, 
                                    this.victims_not_saved_ids,
                                    this.user_config.yellow_victim_count);

        // display player score
        document.getElementById("score").textContent = this.player_score;



        /*=============================================================== */
        /*========================== CREATE AGENTS  ====================== */
        /*================================================================ */

        /*============= CREATE AGENTS(s) =============== */


        // loop through player limit and create user player and teammates 
        for (var i = 0; i < this.player_limit; i++) {

            this.player_list[i] = new Player(this, {
                "x": this.game_setup["starting_locations"][i][0],
                "y": this.game_setup["starting_locations"][i][1],
                "name": this.agent_type_list[i],
                "scale": this.game_setup.scale
            });
            this.player_list[i].obj.setPipeline('Light2D');
            this.player_list[i].obj.setCollideWorldBounds(true);

            // set player to collied with objects and static collision and wall layers
            this.physics.add.collider(this.player_list[i].obj, static_collision_layer, null, null, this);
            this.physics.add.collider(this.player_list[i].obj, static_wall_layer, null, null, this);
            this.physics.add.collider(this.player_list[i].obj, this.green_victims, null, null, this);
            this.physics.add.collider(this.player_list[i].obj, this.yellow_victims, null, null, this);

        }


        /*============= MISC. SET UP =============== */

        // set world bounds so the little player cant walk into imaginary space :) 
        this.physics.world.setBounds(0, 0, this.tile_size * this.map.width,this.tile_size * this.map.height)

        // cursors
        this.cursors = this.input.keyboard.createCursorKeys();
        this.input.keyboard.removeCapture('SPACE, SHIFT');

        // camera
        this.cameras.main.setBounds(0, 0, this.tile_size * this.map.width, this.tile_size * this.map.height);
        this.cameras.main.startFollow(this.player_list[this.player_id], false);
        this.cameras.main.zoom = 5.5;

        // // keys 
        this.keys = this.input.keyboard.addKeys('R,T');

        // this.physics.world.fixedStep = true; // default
        // this.physics.world.fixedStep = false;  

        this._light = this.lights.addLight(500, 200, 80);
        this.lights.enable().setAmbientColor(0x000000);

        // this.fov = new Mrpas(this.map.width this.map.height, (x, y) => {
        //     const tile = static_wall_layer.getTileAt(x, y)
        //     return tile && !tile.collides
        // })



        // switch between DOM text input and game cursor and keys for experiment 3
        document.getElementById("player_msg").addEventListener("focusin",  () => {
            this.input.keyboard.enabled = false;
            this.input.keyboard.disableGlobalCapture();
        });
        document.getElementById("player_msg").addEventListener("focusout", () => {
            this.input.keyboard.enabled = true;
            this.input.keyboard.enableGlobalCapture();
            console.log("enable global");
        });
        document.getElementById("game").addEventListener("click", () => {
            console.log("blur click");
            document.getElementById("player_msg").blur();
        });     

        // socket for moving player 
        this.socket.on('player_move', (message) => { this.game_state.playerMove(message) });

        // socket for teammate rescue
        this.socket.on('rescue_success', (message) => { this._successful_rescue(message) });

        // socket for teammate rescue failure
        this.socket.on('rescue_failure', (message) => { this._failed_rescue(message) });

        // socket for end game
        this.socket.on('end_game', (message) => { this._end_game(message) });

        this.asist_timer.startCountdown();
        
        this.posInterval = setInterval(this._store_position.bind(this), 200);
    }

    update() {

        if (this.asist_timer.DONE == true) {
            //socket emit game end
            var message =  { "topic" : "mission_end",
                            "key": "timer_up", 
                            "publisher" : "client",
                            "rm_id": this.room_id,
                            };

            this.socket.emit('end_game', message);
            this._end_game(message);

        } else {
            this._light.x = this.player_list[this.player_id].obj.body.position.x;
            this._light.y = this.player_list[this.player_id].obj.body.position.y;
            if (this.cursors.up.isDown) {
                this._emit_move(0, -1 * this.speed, 'up')
            } else if (this.cursors.down.isDown) {
                this._emit_move(0, this.speed, 'down')
            } else if (this.cursors.left.isDown) {
                this._emit_move(-1 * this.speed, 0, 'left')
            } else if (this.cursors.right.isDown) {
                this._emit_move(this.speed, 0, 'right')
            } else {
                this._emit_move(0, 0, 'down')
            }
            this._rescue();

            // for experiment 3
            // if (Phaser.Input.Keyboard.JustUp(this.keys.T)) {
            //     document.getElementById("player_msg").focus();
            // }
        }
    }

    _store_position() {

        this.socket.emit("store_position", {
            "topic": "observation",
            "key": "loc",
            "publisher": "client",
            "x": this.player_list[this.player_id].obj.x,
            "y": this.player_list[this.player_id].obj.y,
            "rm_id": this.room_id,
            "idx": this.player_id,
        });

    }

    _emit_move(x_vel, y_vel, anim) {

        this.player_list[this.player_id].move(x_vel, y_vel, this.agent_type_list[this.player_id] + anim)


        this.socket.emit("player_move", {
            "topic": "observation",
            "key": "move",
            "publisher": "client",
            "x": this.player_list[this.player_id].obj.x,
            "y": this.player_list[this.player_id].obj.y,
            "x_vel": x_vel,
            "y_vel": y_vel,
            "rm_id": this.room_id,
            "idx": this.player_id,
            "anim": anim,
        });

    }
    _rescue() {
        if (Phaser.Input.Keyboard.JustDown(this.keys.R)) {
            var x1 = this.player_list[this.player_id].obj.body.position.x;
            var y1 = this.player_list[this.player_id].obj.body.position.y;
            this.socket.emit("rescue_pressed", {
                "topic": "event",
                "key": "rescue_pressed",
                "publisher": "client",
                "x" : x1,
                "y" : y1, 
                "idx": this.player_id, 
                "med_kits_used_before_pl": this.player_med_kits_used,
                "green_vic_saved_before_pl" : this.player_green_saved, 
                "yellow_vic_saved_before_pl" : this.player_yellow_saved,
                "victims_left_team": Array.from(this.victims_not_saved_ids).length,
            });
            this._rescue_victim_type(this.green_victims,x1,y1);
            this._rescue_victim_type(this.yellow_victims,x1,y1);
        }
    }
    _rescue_victim_type(victims,x1,y1) {

        for (var i = victims.children.entries.length - 1; i >= 0; i--) {
            var x2 = victims.children.entries[i].body.position.x;
            var y2 = victims.children.entries[i].body.position.y;
            var a = x1 - x2;
            var b = y1 - y2;
            var c = Math.sqrt(a * a + b * b);
            if (c < this.min_distance_away_from_victim_to_save) {

                var vic_id = victims.children.entries[i].id;

                // Emit rescue to check if there are enough medkits
                this.socket.emit("rescue_attempt", {
                    "topic": "event",
                    "key": "rescue_attempt",
                    "publisher": "client",
                    "x" : x1,
                    "y" : y1, 
                    "idx": this.player_id, 
                    "vx" : x2,
                    "vy" : y2,
                    "victim_id": vic_id,
                    "med_kits_used_before_pl": this.player_med_kits_used,
                });
            }

        }
        
    }

    _remove_victim(victim_id, victim_objs, victim_type, points_per_victim, player_id_rescue, medkits_need_to_rescue) {
        // remove victim from obj list 
        for (var i = victim_objs.children.entries.length - 1; i >= 0; i--) {
            var id =  victim_objs.children.entries[i].id;

            if (id == victim_id) {

                this.victims_not_saved_ids.delete(id)
                this.victims_saved_ids.add(id)

                //
                var x = victim_objs.children.entries[i].body.position.x;
                var y = victim_objs.children.entries[i].body.position.y;

                if (victim_type.charAt(0) == "y") {
                    var img = this.add.image(x, y, 'yellow_victim_saved').setOrigin(0, 0)
                    img.setScale(0.1, 0.1);
                } else if (victim_type.charAt(0) == "g") {
                    var img = this.add.image(x, y, 'green_victim_saved').setOrigin(0, 0)
                    img.setScale(0.1, 0.1);
        
                } 

                victim_objs.children.entries[i].destroy();
        
                // only update the score 
                if (this.player_id == player_id_rescue){
                    // Update medkits used and score 

                    this.player_med_kits_used += parseInt(medkits_need_to_rescue);
                    this.player_score += points_per_victim;

                    // Update medkits used and score in UI 
                    document.getElementById("player-med-kits-used").textContent = this.player_med_kits_used;
                    document.getElementById("score").textContent = this.player_score;


                    if (victim_type.charAt(0) == "y") {
                        this.player_yellow_saved += 1
                        document.getElementById("yellow_victim_saved").textContent = this.player_yellow_saved;
                    } else if (victim_type.charAt(0) == "g") {
                        this.player_green_saved += 1
                        document.getElementById("green_victim_saved").textContent = this.player_green_saved;

                    }
                }
                // might not work for n>2 team
                else {
                    // Update medkits used and score 

                    this.teammate_med_kits_used += parseInt(medkits_need_to_rescue);
                    this.teammate_score += points_per_victim;

                    // Update medkits used and score in UI 
                    document.getElementById("teammate-medkit-progress").textContent = this.teammate_med_kits_used; // todo
                    document.getElementById("teammate-score-progress").textContent = this.teammate_score; // todo


                    if (victim_type.charAt(0) == "y") {
                        this.teammate_yellow_saved += 1
                        document.getElementById("teammate-yellow-vic-progress").textContent = this.teammate_yellow_saved; // todo
                    } else if (victim_type.charAt(0) == "g") {
                        this.teammate_green_saved += 1
                        document.getElementById("teammate-green-vic-progress").textContent = this.teammate_green_saved; // todo

                    }

                }
                document.getElementById("team-med-kits-used").textContent = this.player_med_kits_used + this.teammate_med_kits_used;
                document.getElementById("team_victim_saved").textContent = this.player_score/10 + this.teammate_score/10;

            }

        }
    }

    _successful_rescue(message) {
        // console.log(message)
        //  want to parse message and removed the victim with saved id "victim_id"
        var victim_id = message["victim_id"];
        // if victim is yellow remove from yellow objs
        if (victim_id.charAt(1) == "y") {
            this._remove_victim(victim_id, this.yellow_victims, "yellow", this.user_config.yellow_victim_point, message["idx"], message["medkits_used"])
        }
        // if victim is green remove from green objs 
        else if (victim_id.charAt(1) == "g") {
            this._remove_victim(victim_id, this.green_victims, "green", this.user_config.green_victim_point, message["idx"], message["medkits_used"])

        }
    }

    _failed_rescue(message) {
        if (this.player_id == message["idx"]) {
            // blink medkit when there is not enough to save victims
            document.getElementById("medkit-icon-top").classList.toggle("blink-bg");
            document.getElementById("player-med-kits-used").classList.toggle("blink-bg");
            document.getElementById("used-medkit-label").classList.toggle("blink-bg");
            // toggle after 3 seconds 
            setTimeout(
                function() {
                    document.getElementById("medkit-icon-top").classList.toggle("blink-bg");
                    document.getElementById("player-med-kits-used").classList.toggle("blink-bg");
                    document.getElementById("used-medkit-label").classList.toggle("blink-bg");
                }, 3000);
        }
    }

    _victims_die() {
        let victims = this.yellow_victims 
        console.log("remaining victims are dead")
        for (var i = victims.children.entries.length - 1; i >= 0; i--) {
            var x = victims.children.entries[i].body.position.x
            var y = victims.children.entries[i].body.position.y
            var img = this.add.image(x, y, 'yellow_victim_dead').setOrigin(0, 0)
            img.setScale(0.1, 0.1);
            victims.children.entries[i].destroy();
        }

    }

    _end_game(message) {
        clearInterval(this.posInterval);
        this.input.keyboard.clearCaptures();
        this.game.scene.stop("ASISTScene");

        var game_over_msg = "<span style = 'font-size : xxx-large; \
                                        color : #fff;'> \
                                        <br><br><br><b>G A M E  O V E R</b><br><br>";
        if (message["key"] == "all_victims_saved") {
            game_over_msg += "Saved all victims!";
        } else if (message["key"] == "all_medkits_used") {
            game_over_msg += "Team ran out of<br> medical kits!";
        } else if (message["key"] == "timer_up") {
            game_over_msg += "Timer is up!";
        }

        document.getElementById("game").style.textAlign = "center";
        document.getElementById("game").innerHTML = game_over_msg + "</span>";

        setTimeout(() => {this.testbed.end_game_callback()}, 2000); 
    }
};