export default class GameState {

    constructor(config) {
        this.scene = config.scene;
        this.messages = new Array(this.scene.player_limit).fill(0);
        this.counts = new Array(this.scene.player_limit).fill(0);
    }

    playerMove = function(message) {

        if (message.idx != this.scene.player_id) {
            this.messages[message.idx] = this.messages[message.idx] + 1

            if (this.messages[message.idx] > 10) {
                this.counts[message.idx] = this.counts[message.idx] + 1

                if (this.counts[message.idx] = 5) {
                    this.scene.player_list[message.idx].obj.setPosition(message["x"], message["y"])
                    this.counts[message.idx] = 0
                }

                this.scene.player_list[message.idx].move(message["x_vel"], message["y_vel"], this.scene.agent_type_list[message.idx] + message["anim"])
            }

        }


    }
}