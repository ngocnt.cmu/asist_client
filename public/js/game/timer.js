// TIMER   --- Credit: Mateusz Rybczonec

export default class GameTimer {

    constructor(time_limit, element_id) {
        this.TIME_LIMIT = time_limit;
        this.FULL_DASH_ARRAY = 283;
        this.WARNING_THRESHOLD = Math.round((2 * this.TIME_LIMIT) / 3);
        this.ALERT_THRESHOLD = Math.round((1 * this.TIME_LIMIT) / 3);

        this.START = null;
        this.DONE = false;
        this.timer_element_id = element_id;
        this.timePassed = 0;
        this.timeLeft = this.TIME_LIMIT;
        this.timerInterval = null;
    }

    startCountdown() {
        this.START = Date.now();
        this.startTimer();
    }

    onTimesUp() {
        this.DONE = true;
        clearInterval(this.timerInterval);
    }

    current_time() {
        this.timePassed = Math.floor((Date.now() - this.START) / 1000);
        return Date.now() - this.START;
    }

    startTimer() {
        this.timerInterval = setInterval(() => {
            this.timePassed = Math.floor((Date.now() - this.START) / 1000);

            this.timeLeft = this.TIME_LIMIT - this.timePassed;
            document.getElementById(this.timer_element_id).innerHTML =
                this.formatTime(this.timeLeft);

            if (this.timeLeft <= 0) {
                this.onTimesUp();
            }
        }, 1000);

        if (this.DONE) {
            clearInterval(this.timerInterval);
        }
    }

    formatTime(time) {
        const minutes = Math.floor(time / 60);
        let seconds = time % 60;

        if (seconds < 10) {
            seconds = `0${seconds}`;
        }

        return `${minutes}:${seconds}`;
    }
}