import { pre_survey_yellow, pre_survey_green } from "/js/surveys/pre_survey.js"
import { post_survey } from "/js/surveys/post_survey.js"

Survey.StylesManager.applyTheme("defaultV2");


export function display_pre_survey(player_id, callback) {

    var pre_survey = [pre_survey_green, pre_survey_yellow][player_id];

    var survey = new Survey.Model(pre_survey);
    $("#pre-survey-container").Survey({model: survey});

    survey.onComplete.add(function() {
        callback(survey.data);
    });
}

export function display_post_survey(callback) {

    var survey = new Survey.Model(post_survey);
    $("#post-survey-container").Survey({model: survey});

    survey.onComplete.add(function(survey){
        callback(survey.data);
    });
}