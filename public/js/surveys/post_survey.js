var post_survey = {
    completedHtml: "<h3>This marks the end of our study. Thank you for your participation!</h3><br>\
    <h4><a href='https://app.prolific.co/submissions/complete?cc=814004C8'>Please click here to access your completion code for Prolific.</a></h4>",
    showQuestionNumbers: "off",
    pages: [
        {
            name: "page1",
            elements: [
                {
                    type: "radiogroup",
                    name: "strategy_player",
                    isRequired: true,
                    title: "Which of the following best describes your strategy in the game?",
                    choices: [
                        { "value": "green", "text": "Prioritize saving Green Victims" },
                        { "value": "yellow", "text": "Prioritize saving Yellow Victims" }, 
                        { "value": "all", "text": "Save any victims." }, 
                        { "value": "none", "text": "I didn't have a strategy." }]

                },
                {
                    type: "radiogroup",
                    name: "strategy_teammate",
                    isRequired: true,
                    title: "Which of the following best describes your teammate's strategy during the game?",
                    choices: [
                        { "value": "green", "text": "Prioritize saving Green Victims." },
                        { "value": "yellow", "text": "Prioritize saving Yellow Victims." }, 
                        { "value": "all", "text": "Save any victim" }, 
                        { "value": "none", "text": "They didn't have a strategy." }]

                },
                {
                    type: "comment",
                    name: "strategy_player_txt",
                    isRequired: true,
                    title: "Please elaborate on your strategy during the game, and why you chose it.",
                    placeHolder: "This field is required.",

                },
                {
                    type: "rating",
                    name: "performanceSatisfaction-player",
                    isRequired: true,
                    title: "How satisfied are you\
                    with YOUR ability to save victims?",
                    autoGenerate: false,
                    rateCount: 5,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Unsatisfied"
                    },
                    {
                        "value": 2,
                        "text": "Somewhat Unsatisfied"
                    },
                    {
                        "value": 3,
                        "text": "Satisfied nor Unsatisfied"
                    },
                    {
                        "value": 4,
                        "text": "Somewhat Satisfied"
                    },
                    {
                        "value": 5,
                        "text": "Satisfied"
                    }]
                },
                {
                    type: "rating",
                    name: "performanceSatisfaction-teammate",
                    isRequired: true,
                    title: "How satisfied are you\
                    with your TEAMMATE's ability to save victims?",
                    autoGenerate: false,
                    rateCount: 5,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Unsatisfied"
                    },
                    {
                        "value": 2,
                        "text": "Somewhat Unsatisfied"
                    },
                    {
                        "value": 3,
                        "text": "Satisfied nor Unsatisfied"
                    },
                    {
                        "value": 4,
                        "text": "Somewhat Satisfied"
                    },
                    {
                        "value": 5,
                        "text": "Satisfied"
                    }]
                },
                {
                    type: "comment",
                    name: "performanceSatisfaction_txt",
                    isRequired: true,
                    title: "Please explain your satisfaction levels for you and your teammate's performance",
                    placeHolder: "This field is required.",

                },
                
            ]
        },

        {
            name: "page2",
            elements: [
                {
                    type: "rating",
                    name: "trustworthiness-1",
                    isRequired: true,
                    title: "My teammate shows a great deal of integrity.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-2",
                    isRequired: true,
                    title: "I can rely on my teammate.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-3",
                    isRequired: true,
                    title: "Overall, my teammate is very trustworthy.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-4",
                    isRequired: true,
                    title: "We are usually considerate of one another's feelings in this group.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-5",
                    isRequired: true,
                    title: "My teammate is friendly.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-6",
                    isRequired: true,
                    title: "There is no 'team spirit' in our group.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 7,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 6,
                        "text": "Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 2,
                        "text": "Agree"
                    },
                    {
                        "value": 1,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-7",
                    isRequired: true,
                    title: "There is a noticeable lack of confidence among those with whom I work.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 7,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 6,
                        "text": "Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 2,
                        "text": "Agree"
                    },
                    {
                        "value": 1,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trustworthiness-8",
                    isRequired: true,
                    title: "My teammate and I have confidence in one another.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
            ]
        },
        {
            name: "page3",
            elements: [
                {
                    type: "rating",
                    name: "trust-1",
                    isRequired: true,
                    title: "If I had my way, I wouldn't let my teammate have any influence over issues that are important to the task.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 7,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 6,
                        "text": "Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 2,
                        "text": "Agree"
                    },
                    {
                        "value": 1,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trust-2",
                    isRequired: true,
                    title: "I would be comfortable giving the other team members complete responsibility for the completion of the task.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
                {
                    type: "rating",
                    name: "trust-3",
                    isRequired: true,
                    title: "I really wish I had a good way to oversee the work of my teammate during the game.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 7,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 6,
                        "text": "Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 2,
                        "text": "Agree"
                    },
                    {
                        "value": 1,
                        "text": "Strongly Agree"
                    }],
                },
                {
                    type: "rating",
                    name: "trust-4",
                    isRequired: true,
                    title: "I would be comfortable giving my teammate a task that was critical to our game, even if I could not monitor them.",
                    autoGenerate: false,
                    rateCount: 7,
                    rateValues: [
                    {
                        "value": 1,
                        "text": "Strongly Disagree"
                    },
                    {
                        "value": 2,
                        "text": "Disagree"
                    },
                    {
                        "value": 3,
                        "text": "Somewhat Disagree"
                    },
                    {
                        "value": 4,
                        "text": "Agree nor Disagree"
                    },
                    {
                        "value": 5,
                        "text": "Somewhat Agree"
                    },
                    {
                        "value": 6,
                        "text": "Agree"
                    },
                    {
                        "value": 7,
                        "text": "Strongly Agree"
                    }]
                },
            ]
        },
        {
            name: "page4",
            elements: [
                {
                    type: "comment",
                    name: "future_txt",
                    isRequired: true,
                    title: "How do you think your team could collaborate BETTER in the future?",
                    placeHolder: "This field is required.",

                }
            ]
        },

    ],
    "showCompletedPage": true,
    "showProgressBar": "bottom",
    "completeText": "Next",
    "showQuestionNumbers": "off",
    "widthMode": "static",
    "width": "750px"
};

export {post_survey}
