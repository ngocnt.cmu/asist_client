var pre_survey_yellow = {
    "title": "Quiz", 
    "description": "This quiz is based on the instructions of the game that you read on the previous page.",
    "pages": [{
        "name": "page1",
        "elements": [
            { "type": "radiogroup", 
            "name": "question1", 
            "title": "Which player are you?", 
            "colCount": 2,
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question1} = item2"
                }
               ],
            "isRequired": true, 
            "choices": [
                { "value": "item1", "text": "Player Green" }, 
                { "value": "item2", "text": "Player Yellow"}] 
            }, 
            { "type": "radiogroup", 
            "name": "question2", 
            "title": "How many medical kits does the team have?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question2} = item5"
                }
               ],
            "isRequired": true, 
            "colCount": 4,
            "choices": [
                { "value": "item2", "text": "15" },  
                { "value": "item3", "text": "20" },
                { "value": "item4", "text": "25" },
                { "value": "item5", "text": "30" }]
            }, 
            { "type": "radiogroup", 
            "name": "question3", 
            "title": "How many green victims will there be?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question3} = item4"
                }
               ],
            "isRequired": true, 
            "colCount": 4,
            "choices": [
                { "value": "item1", "text": "0" },
                { "value": "item2", "text": "5" },  
                { "value": "item3", "text": "10" },
                { "value": "item4", "text": "15" }]
            }, 
            { "type": "radiogroup", 
            "name": "question4", 
            "title": "How many yellow victims will there be?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question4} = item4"
                }
               ],
            "isRequired": true, 
            "colCount": 4,
            "choices": [
                { "value": "item1", "text": "0" },
                { "value": "item2", "text": "5" },  
                { "value": "item3", "text": "10" },
                { "value": "item4", "text": "15" }]
            },
            { "type": "radiogroup", 
            "name": "question5-0", 
            "title": "Which key do you need to press to save victims?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question5-0} = item3"
                }
               ],
            "isRequired": true, 
            "colCount": 4,
            "choices": [
                { "value": "item1", "text": "k" },
                { "value": "item2", "text": "v" }, 
                { "value": "item3", "text": "r" },
                { "value": "item4", "text": "space" }]
            },
        ]             
    },
    {
        "name": "page2",
        "elements": [
            { "type": "radiogroup", 
            "name": "question5", 
            "title": "How many medical kits do YOU need to save a GREEN Victim?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question5} = item2"
                }
               ],
            "isRequired": true, 
            "colCount": 3,
            "choices": [
                { "value": "item1", "text": "0" },
                { "value": "item2", "text": "1" }, 
                { "value": "item3", "text": "2" }]
            },         
            { "type": "radiogroup", 
            "name": "question6", 
            "title": "How many medical kits do YOU need to save a YELLOW Victim?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question6} = item3"
                }
               ],
            "isRequired": true, 
            "colCount": 3,
            "choices": [
                { "value": "item1", "text": "0" },
                { "value": "item2", "text": "1" }, 
                { "value": "item3", "text": "2" }]
            },  

            { "type": "radiogroup", 
            "name": "question8", 
            "title": "How much would you and your teammate earn if YOU save a GREEN victim?", 
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question8} = item1"
                }
               ],
            "isRequired": true, 
            "choices": [
                { "value": "item1", "text": "We would both earn $.1" },
                { "value": "item2", "text": "We would both earn $.15" }, 
                { "value": "item3", "text": "I would earn $0 and my teammate would earn $.3" }, 
                { "value": "item4", "text": "I would earn $.3 and my teammate would earn $0" }]
            },         

            { "type": "radiogroup", 
            "name": "question9", 
            "title": "How much would you and your teammate earn if YOU save a YELLOW victim?",  
            "validators": [
                {
                 "type": "expression",
                 "text": "Incorrect Answer. Please answer again.",
                 "expression": "{question9} = item4"
                }
               ],
            "isRequired": true, 
            "choices": [
                { "value": "item1", "text": "We would both earn $.1" },
                { "value": "item2", "text": "We would both earn $.15" }, 
                { "value": "item3", "text": "I would earn $0 and my teammate would earn $.3" }, 
                { "value": "item4", "text": "I would earn $.3 and my teammate would earn $0" }]
            },  
        ]             
    },
    {
        "name": "page3",
        "elements": [
            { "type": "radiogroup", 
            "name": "player_prestrategy", 
            "title": "Please select an option closest to your intended strategy.", 
            "isRequired": true, 
            "choices": [
                { "value": "green", "text": "Prioritize saving Green Victims" },
                { "value": "yellow", "text": "Prioritize saving Yellow Victims" }, 
                { "value": "all", "text": "Save any victims." }, 
                { "value": "none", "text": "I don't have a strategy." }]
            }, 
            { "type": "radiogroup", 
            "name": "teammate_prestrategy", 
            "title": "Please provide a best guess of your teammate's intended strategy", 
            "isRequired": true, 
            "choices": [
                { "value": "green", "text": "Prioritize saving Green Victims" },
                { "value": "yellow", "text": "Prioritize saving Yellow Victims" }, 
                { "value": "all", "text": "Save any victims." }, 
                { "value": "none", "text": "No strategy" }]
            }, 
            { "type": "comment", 
            "name": "strategy_reason_txt_presurvey", 
            "title": "Please provide a reason for your strategy", 
            "isRequired": true, 
            "placeHolder": "This field is required.",
            },
        ]             
    },
    ],
    "showCompletedPage": false,
    "showQuestionNumbers": "on",
    "showProgressBar": "both",
    "progressBarType": "questions",
    "completeText": "Next",
    "widthMode": "static",
    "width": "750px"

}


var pre_survey_green = JSON.parse(JSON.stringify(pre_survey_yellow));

pre_survey_green["pages"][0]["elements"][0]["validators"][0]["expression"] = "{question1} = item1"
pre_survey_green["pages"][1]["elements"][0]["validators"][0]["expression"] = "{question5} = item3"
pre_survey_green["pages"][1]["elements"][1]["validators"][0]["expression"] = "{question6} = item2"
pre_survey_green["pages"][1]["elements"][2]["validators"][0]["expression"] = "{question8} = item4"
pre_survey_green["pages"][1]["elements"][3]["validators"][0]["expression"] = "{question9} = item1"


export {pre_survey_yellow, pre_survey_green};