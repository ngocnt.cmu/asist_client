export class ArraySet extends Set {
    add(arr) {
        super.add(arr.toString());
    }
}

export function seconds_string(time) {

    var minutes = Math.floor(time / 60);
    var seconds = time - minutes * 60;

    if (seconds == 0 && minutes <= 1) {
        return String(minutes) + " minute"
    } else if (seconds == 0 && minutes > 1) {
        return String(minutes) + " minutes"
    } else if (seconds > 0 && minutes == 0) {
        return String(seconds) + " seconds"
    } else {
        return String(minutes) + " minutes " + String(seconds) + " seconds"
    }
}